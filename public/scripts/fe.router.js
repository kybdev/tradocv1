'use strict';
// app.config(function($locationProvider) {
//     $locationProvider.html5Mode({
//         enabled: true,
//         requireBase: false
//     });
//     $locationProvider.hashPrefix('');
//
//     // $ocLazyLoad.load([
//     //   '../bower_components/sweetalert/dist/sweetalert.min.js',
//     // ]);
//
//     // $ocLazyLoad.load({
//     //   "name" : "angularMoment",
//     //   "files":["bower_components/angular-moment/angular-moment.min.js"]
//     // });
//
//
// });

/**
* Config for the router
*/
app.config(['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$ocLazyLoadProvider', 'JS_REQUIRES','$interpolateProvider','$httpProvider','$locationProvider',
function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $ocLazyLoadProvider, jsRequires, $interpolateProvider,$httpProvider,$locationProvider ) {

    app.controller = $controllerProvider.register;
    app.directive = $compileProvider.directive;
    app.filter = $filterProvider.register;
    app.factory = $provide.factory;
    app.service = $provide.service;
    app.constant = $provide.constant;
    app.value = $provide.value;

    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');


}]);
