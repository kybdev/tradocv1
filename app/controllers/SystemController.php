<?php

use Phalcon\Mvc\View;

class SystemController extends ControllerBase
{

    public function indexAction(){
    	$this->view->setRenderLevel(View::LEVEL_ACTION_VIEW);
    }
}
